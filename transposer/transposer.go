package transposer

import (
	"fmt"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"

	matmod "gonum.org/v1/gonum/mat"
)

type TTransposer struct {
	rows int
	cols int
	mat  *matmod.Dense
}

func NewTransposer(
	origRows int,
	origCols int,
) *TTransposer {
	mat := matmod.NewDense(
		origCols,
		origRows,
		nil,
	)

	return &TTransposer{
		rows: origCols,
		cols: origRows,
		mat:  mat,
	}
}

func (self *TTransposer) Transpose(
	mat *matmod.Dense,
) *matmod.Dense {
	r, c := mat.Dims()
	if r != self.cols || c != self.rows {
		errorsutils.PanicError(
			"Transposer",
			"Transpose",
			fmt.Errorf(
				"Dims mismatch: got (%v, %v), expected (%v, %v)",
				r,
				c,
				self.cols,
				self.rows,
			),
		)

	}

	for i := 0; i < c; i += 1 {
		for j := 0; j < r; j += 1 {
			self.mat.Set(i, j, mat.At(j, i))
		}
	}

	return self.mat
}
