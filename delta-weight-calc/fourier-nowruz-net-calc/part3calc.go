package fouriernowruznetcalc

import (
	matmod "gonum.org/v1/gonum/mat"
	math "math"
)

type TFourierNowruzPart3Calc struct {
	currDim int
	onesVec *matmod.Dense
	result  *matmod.Dense
}

func NewFourierNowruzPart3Calc(currDim int) *TFourierNowruzPart3Calc {
	ones := make([]float64, currDim)
	for i := range ones {
		ones[i] = 1.0
	}
	onesVec := matmod.NewDense(
		currDim,
		1,
		ones,
	)

	return &TFourierNowruzPart3Calc{
		currDim: currDim,
		onesVec: onesVec,
		result: matmod.NewDense(
			currDim,
			1,
			nil,
		),
	}
}

// Key component: pi * (sum(j, k) ^ 2 + 1)
func (self *TFourierNowruzPart3Calc) Calc(
	currSumOutputs *matmod.Dense,
) *matmod.Dense {
	self.result.MulElem(
		currSumOutputs,
		currSumOutputs,
	)
	self.result.Add(
		self.result,
		self.onesVec,
	)
	self.result.Scale(
		math.Pi,
		self.result,
	)

	return self.result
}
