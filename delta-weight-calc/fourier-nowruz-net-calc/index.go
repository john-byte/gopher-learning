package fouriernowruznetcalc

import (
	matmod "gonum.org/v1/gonum/mat"
)

type TFourierNowruzNetCalc struct {
	currDim   int
	prevDim   int
	part1Calc *TFourierNowruzPart1Calc
	part3Calc *TFourierNowruzPart3Calc
	tempVec   *matmod.Dense
	result    *matmod.Dense
}

func New(
	currDim int,
	prevDim int,
) *TFourierNowruzNetCalc {
	zerosTempVec := make([]float64, currDim)
	for i := range zerosTempVec {
		zerosTempVec[i] = 0.0
	}

	zerosResult := make([]float64, currDim*prevDim)
	for i := range zerosResult {
		zerosResult[i] = 0.0
	}

	return &TFourierNowruzNetCalc{
		part1Calc: NewFourierNowruzPart1Calc(currDim, prevDim),
		part3Calc: NewFourierNowruzPart3Calc(currDim),
		tempVec: matmod.NewDense(
			currDim,
			1,
			zerosTempVec,
		),
		result: matmod.NewDense(
			currDim,
			prevDim,
			zerosResult,
		),
	}
}

func (self *TFourierNowruzNetCalc) CalcDeltaAmplitude(
	currErrors *matmod.Dense,
	currSumOutputs *matmod.Dense,
	frequencies *matmod.Dense,
	inputs *matmod.Dense,
	learningRate float64,
) *matmod.Dense {
	part1 := self.part1Calc.Calc(frequencies, inputs)
	part3 := self.part3Calc.Calc(currSumOutputs)

	self.tempVec.DivElem(
		currErrors,
		part3,
	)

	for r := 0; r < self.currDim; r += 1 {
		for c := 0; c < self.prevDim; c += 1 {
			res := part1.At(r, c) * self.tempVec.At(r, 0)
			self.result.Set(r, c, res)
		}
	}

	self.result.Scale(
		learningRate,
		self.result,
	)

	return self.result
}
