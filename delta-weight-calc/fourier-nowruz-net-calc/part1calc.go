package fouriernowruznetcalc

import (
	mathutils "gitlab.com/john-byte/gopher-learning/utils/math"
	matmod "gonum.org/v1/gonum/mat"
)

type TFourierNowruzPart1Calc struct {
	currDim int
	prevDim int
	result  *matmod.Dense
}

func NewFourierNowruzPart1Calc(currDim int, prevDim int) *TFourierNowruzPart1Calc {
	return &TFourierNowruzPart1Calc{
		currDim: currDim,
		prevDim: prevDim,
		result: matmod.NewDense(
			currDim,
			prevDim,
			nil,
		),
	}
}

// Key component: (s(j, k) - c(j, k) * f(j, k) * x_in(k) + 1)
func (self *TFourierNowruzPart1Calc) Calc(
	frequencies *matmod.Dense,
	inputs *matmod.Dense,
) *matmod.Dense {
	for r := 0; r < self.currDim; r += 1 {
		for c := 0; c < self.prevDim; c += 1 {
			freq := frequencies.At(r, c)
			inp := inputs.At(c, 0)
			sinp := mathutils.Sinp(freq * inp)
			cosp := mathutils.Cosp(freq * inp)
			res := sinp - cosp*freq*inp + 1.0

			self.result.Set(r, c, res)
		}
	}

	return self.result
}
