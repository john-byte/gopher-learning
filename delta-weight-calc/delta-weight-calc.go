package deltaweightcalc

import (
	matmod "gonum.org/v1/gonum/mat"
)

type TDeltaWeightCalc interface {
	CalcDeltaWeight(
		currErrors *matmod.Dense,
		currOutputs *matmod.Dense,
		prevOutputs *matmod.Dense,
		learningRate float64,
	) *matmod.Dense
}
