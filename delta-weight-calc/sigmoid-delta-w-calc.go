package deltaweightcalc

import (
	matmod "gonum.org/v1/gonum/mat"
)

type TSigmoidDeltaWeightCalc struct {
	currDim  int
	prevDim  int
	onesVec  *matmod.Dense
	multTemp *matmod.Dense
	result   *matmod.Dense
}

func NewSigmoidDeltaWeightCalc(
	currDim int,
	prevDim int,
) *TSigmoidDeltaWeightCalc {
	ones := make([]float64, currDim)
	for i := range ones {
		ones[i] = 1.0
	}
	onesMat := matmod.NewDense(
		currDim,
		1,
		ones,
	)

	return &TSigmoidDeltaWeightCalc{
		currDim: currDim,
		prevDim: prevDim,
		onesVec: onesMat,
		multTemp: matmod.NewDense(
			currDim,
			1,
			nil,
		),
		result: matmod.NewDense(
			currDim,
			prevDim,
			nil,
		),
	}
}

func (self *TSigmoidDeltaWeightCalc) CalcDeltaWeight(
	currErrors *matmod.Dense,
	currOutputs *matmod.Dense,
	prevOutputs *matmod.Dense,
	learningRate float64,
) *matmod.Dense {
	self.multTemp.Sub(
		self.onesVec,
		currOutputs,
	)
	self.multTemp.MulElem(
		currOutputs,
		self.multTemp,
	)
	self.multTemp.MulElem(
		currErrors,
		self.multTemp,
	)

	self.result.Product(
		self.multTemp,
		prevOutputs,
	)
	self.result.Scale(
		learningRate,
		self.result,
	)

	return self.result
}
