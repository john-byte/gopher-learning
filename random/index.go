package randutils

import (
	"gonum.org/v1/gonum/stat/distuv"
	"math"
)

// randomly generate a float64 array
func RandomArray(size int, v float64) (data []float64) {
	dist := distuv.Uniform{
		Min: -1.0 / math.Sqrt(v),
		Max: 1.0 / math.Sqrt(v),
	}

	data = make([]float64, size)
	for i := 0; i < size; i++ {
		// data[i] = rand.NormFloat64() * math.Pow(v, -0.5)
		data[i] = dist.Rand()
	}
	return
}
