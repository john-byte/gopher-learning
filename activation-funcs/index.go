package activationfuncs

import (
	"math"
)

func Sigmoid(r, c int, z float64) float64 {
	return 1.0 / (1.0 + math.Exp(-1.0*z))
}

func Atanp(r, c int, z float64) float64 {
	return math.Atan(z)/math.Pi + 0.5
}
