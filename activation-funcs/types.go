package activationfuncs

type TActivationFunc func(r, c int, z float64) float64
