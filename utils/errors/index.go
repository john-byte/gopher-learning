package errorsutils

import (
	"fmt"
)

func LogError(
	moduleName string,
	methodName string,
	err error,
) error {
	return fmt.Errorf(
		"[%v / %v] => %v",
		moduleName,
		methodName,
		err.Error(),
	)
}

func PanicError(
	moduleName string,
	methodName string,
	err error,
) {
	panic(
		fmt.Errorf(
			"[%v / %v] => %v",
			moduleName,
			methodName,
			err.Error(),
		).Error(),
	)
}
