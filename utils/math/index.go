package mathutils

import (
	"math"
)

func Atanp(x float64) float64 {
	return math.Atan(x)/math.Pi + 0.5
}

func Sinp(x float64) float64 {
	return math.Sin(x) + 1.0
}

func Cosp(x float64) float64 {
	return math.Cos(x) + 1.0
}
