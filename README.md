This is a library for creating basic neural networks.

## Directories

`delta-weight-calc` - it's a set of calculators for weights correction (such as sigmoid);

`model` - it's a set of neural network's building blocks like layers and model;

`transposer` - it's a calculator for matrix transposition;

`activation-funcs` - it's a set of standard activation functions;

`rand` - it's a set of randomization utils for generating data;

`utils` - it's a set of various other utils needed for above packages;