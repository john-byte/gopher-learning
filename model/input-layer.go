package model

import (
	"errors"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"

	matmod "gonum.org/v1/gonum/mat"
)

type TInputLayer struct {
	dim           int
	outputs       *matmod.Dense
	outputsCalced bool
}

func NewInputLayer(dim int) *TInputLayer {
	return &TInputLayer{
		dim: dim,
		outputs: matmod.NewDense(
			dim,
			1,
			nil,
		),
		outputsCalced: false,
	}
}

func (self *TInputLayer) CalcActivation(inputs *matmod.Dense) *matmod.Dense {
	if self.outputsCalced {
		return self.outputs
	}

	self.outputs = inputs
	self.outputsCalced = true
	return self.outputs
}

func (self *TInputLayer) CalcError(targets *matmod.Dense) *matmod.Dense {
	errorsutils.PanicError(
		"InputLayer",
		"CalcError",
		errors.New("Error can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayer) CalcErrorFromNextLayer(
	weights *matmod.Dense,
	outputs *matmod.Dense,
) *matmod.Dense {
	errorsutils.PanicError(
		"InputLayer",
		"CalcErrorFromNextLayer",
		errors.New("Error can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayer) CorrectWeights(
	learningRate float64,
	prevOutputs *matmod.Dense,
) *matmod.Dense {
	errorsutils.PanicError(
		"InputLayer",
		"CorrectWeights",
		errors.New("Error can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayer) Reset() {
	self.outputsCalced = false
}

func (self *TInputLayer) GetWeights() *matmod.Dense {
	errorsutils.PanicError(
		"InputLayer",
		"GetWeights",
		errors.New("Weights can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayer) GetErrors() *matmod.Dense {
	errorsutils.PanicError(
		"InputLayer",
		"GetErrors",
		errors.New("Errors can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayer) GetDim() int {
	return self.dim
}
