package model

import (
	"errors"
	activations "gitlab.com/john-byte/gopher-learning/activation-funcs"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"

	deltawcalcmod "gitlab.com/john-byte/gopher-learning/delta-weight-calc"
	transposemod "gitlab.com/john-byte/gopher-learning/transposer"
	matmod "gonum.org/v1/gonum/mat"
)

type TStandardLayer struct {
	dim              int
	outputs          *matmod.Dense
	outputsCalced    bool
	weights          *matmod.Dense
	weightsCorrected bool
	errors           *matmod.Dense
	errorsCalced     bool
	deltaWeightCalc  deltawcalcmod.TDeltaWeightCalc
	inputsTransposer *transposemod.TTransposer
	activationFn     activations.TActivationFunc
}

func NewStandardLayer(
	dim int,
	initWeights *matmod.Dense,
	deltaWeightCalc deltawcalcmod.TDeltaWeightCalc,
	activationFn activations.TActivationFunc,
) *TStandardLayer {
	_, wC := initWeights.Dims()

	return &TStandardLayer{
		dim: dim,
		outputs: matmod.NewDense(
			dim,
			1,
			nil,
		),
		outputsCalced:    false,
		weights:          initWeights,
		weightsCorrected: false,
		errors: matmod.NewDense(
			dim,
			1,
			nil,
		),
		errorsCalced:     false,
		deltaWeightCalc:  deltaWeightCalc,
		inputsTransposer: transposemod.NewTransposer(wC, 1),
		activationFn:     activationFn,
	}
}

func (self *TStandardLayer) CalcActivation(inputs *matmod.Dense) *matmod.Dense {
	if self.outputsCalced {
		return self.outputs
	}

	self.outputs.Product(
		self.weights,
		inputs,
	)
	self.outputs.Apply(
		self.activationFn,
		self.outputs,
	)
	self.outputsCalced = true
	return self.outputs
}

func (self *TStandardLayer) CalcError(targets *matmod.Dense) *matmod.Dense {
	if self.errorsCalced {
		return self.errors
	}

	if !self.outputsCalced {
		errorsutils.PanicError(
			"StandardLayer",
			"CalcError",
			errors.New("Outputs is not calculated"),
		)
		return nil
	}

	self.errors.Sub(
		targets,
		self.outputs,
	)
	self.errorsCalced = true
	return self.errors
}

func (self *TStandardLayer) CalcErrorFromNextLayer(
	weights *matmod.Dense,
	errs *matmod.Dense,
) *matmod.Dense {
	if self.errorsCalced {
		return self.errors
	}

	if !self.outputsCalced {
		errorsutils.PanicError(
			"StandardLayer",
			"CalcErrorFromNextLayer",
			errors.New("Outputs is not calculated"),
		)
		return nil
	}

	self.errors.Product(
		matmod.DenseCopyOf(weights).T(),
		errs,
	)
	self.errorsCalced = true
	return self.errors
}

func (self *TStandardLayer) CorrectWeights(
	learningRate float64,
	prevOutputs *matmod.Dense,
) *matmod.Dense {
	if self.weightsCorrected {
		return self.weights
	}

	if !self.outputsCalced {
		errorsutils.PanicError(
			"StandardLayer",
			"CorrectWeights",
			errors.New("Outputs is not calculated"),
		)
		return nil
	}

	if !self.errorsCalced {
		errorsutils.PanicError(
			"StandardLayer",
			"CorrectWeights",
			errors.New("Error is already calculated"),
		)
		return nil
	}

	prevOutsT := self.inputsTransposer.Transpose(
		prevOutputs,
	)
	deltaWeight := self.deltaWeightCalc.CalcDeltaWeight(
		self.errors,
		self.outputs,
		prevOutsT,
		learningRate,
	)

	self.weights.Add(
		self.weights,
		deltaWeight,
	)

	self.weightsCorrected = true
	return self.weights
}

func (self *TStandardLayer) Reset() {
	self.outputsCalced = false
	self.errorsCalced = false
	self.weightsCorrected = false
}

func (self *TStandardLayer) GetWeights() *matmod.Dense {
	return self.weights
}

func (self *TStandardLayer) GetErrors() *matmod.Dense {
	return self.errors
}

func (self *TStandardLayer) GetDim() int {
	return self.dim
}
