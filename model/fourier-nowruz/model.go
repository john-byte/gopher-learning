package fouriernowruzmodel

import (
	"fmt"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"
	"os"
	"path"

	modelmod "gitlab.com/john-byte/gopher-learning/model"
	matmod "gonum.org/v1/gonum/mat"
)

type TFourierNowruzModel struct {
	inputLayer     *modelmod.TInputLayerWithMem
	standardLayers []*TStandardFourierNowruzLayer
	learningRate   float64
	modelPath      string
}

func NewModel(
	inputLayer *modelmod.TInputLayerWithMem,
	standardLayers []*TStandardFourierNowruzLayer,
	learningRate float64,
	modelPath string,
) *TFourierNowruzModel {
	return &TFourierNowruzModel{
		inputLayer:     inputLayer,
		standardLayers: standardLayers,
		learningRate:   learningRate,
		modelPath:      modelPath,
	}
}

func (self *TFourierNowruzModel) Train(
	inputData []float64,
	memoryData []float64,
	targetData []float64,
) {
	inp := self.inputLayer.CalcActivation(
		matmod.NewDense(
			len(inputData),
			1,
			inputData,
		),
		matmod.NewDense(
			len(memoryData),
			1,
			memoryData,
		),
	)

	out := inp
	for i := 0; i < len(self.standardLayers); i += 1 {
		out = self.standardLayers[i].CalcActivation(out)
	}

	targets := matmod.NewDense(len(targetData), 1, targetData)
	layerErr := self.standardLayers[len(self.standardLayers)-1].CalcError(targets)
	for i := len(self.standardLayers) - 2; i >= 0; i -= 1 {
		layerErr = self.standardLayers[i].CalcErrorFromNextLayer(
			self.standardLayers[i+1].GetAmplitudes(),
			layerErr,
		)
	}

	for i := len(self.standardLayers) - 1; i > 0; i -= 1 {
		self.standardLayers[i].CorrectAmpsFreqs(
			self.learningRate,
			self.standardLayers[i-1].CalcActivation(nil),
		)
	}
	self.standardLayers[0].CorrectAmpsFreqs(
		self.learningRate,
		inp,
	)

	for i := len(self.standardLayers) - 1; i >= 0; i -= 1 {
		self.standardLayers[i].Reset()
	}

	self.inputLayer.ResetInput()
	self.inputLayer.ResetMemAtCurrBlock()
}

func (self *TFourierNowruzModel) Predict(inputData []float64, memoryData []float64) matmod.Matrix {
	out := self.inputLayer.CalcActivation(
		matmod.NewDense(
			len(inputData),
			1,
			inputData,
		),
		matmod.NewDense(
			len(memoryData),
			1,
			memoryData,
		),
	)

	for i := 1; i < len(self.standardLayers); i += 1 {
		out = self.standardLayers[i].CalcActivation(out)
	}

	res := matmod.DenseCopyOf(out)

	for _, l := range self.standardLayers {
		l.Reset()
	}

	self.inputLayer.ResetInput()
	self.inputLayer.ResetMemAtCurrBlock()

	return res
}

func (self *TFourierNowruzModel) ExpandMemory() {
	self.inputLayer.ExpandMem()
}

func (self *TFourierNowruzModel) ResetMemory() {
	self.inputLayer.ResetFullMem()
}

func (self *TFourierNowruzModel) Save() error {
	for i, l := range self.standardLayers {
		if i == 0 {
			continue
		}

		modelFileName := fmt.Sprintf(
			"layer-%v.dat",
			i+1,
		)
		modelPath := path.Join(
			self.modelPath,
			modelFileName,
		)
		file, err := os.Create(modelPath)
		defer file.Close()

		if err != nil {
			return errorsutils.LogError(
				"Model",
				"Save",
				err,
			)
		}
		l.GetAmplitudes().MarshalBinaryTo(file)
	}

	return nil
}

func (self *TFourierNowruzModel) Load() error {
	for i, l := range self.standardLayers {
		if i == 0 {
			continue
		}

		modelFileName := fmt.Sprintf(
			"layer-%v.dat",
			i+1,
		)
		modelPath := path.Join(
			self.modelPath,
			modelFileName,
		)
		file, err := os.Open(modelPath)
		defer file.Close()

		if err != nil {
			return errorsutils.LogError(
				"Model",
				"Load",
				err,
			)
		}

		l.Reset()
		l.GetAmplitudes().Reset()
		l.GetAmplitudes().UnmarshalBinaryFrom(file)

		amps := l.GetAmplitudes()
		aR, aC := amps.Dims()
		freqs := matmod.NewDense(
			aR,
			aC,
			nil,
		)
		eg := l.GetEnergyPotential()
		for r := 0; r < aR; r += 1 {
			for c := 0; c < aC; c += 1 {
				freq := eg / amps.At(r, c)
				freqs.Set(r, c, freq)
			}
		}
		l.GetFrequencies().SetRawMatrix(freqs.RawMatrix())
	}

	return nil
}

func (self *TFourierNowruzModel) GetInputLayer() *modelmod.TInputLayerWithMem {
	return self.inputLayer
}

func (self *TFourierNowruzModel) GetStandardLayers() []*TStandardFourierNowruzLayer {
	return self.standardLayers
}
