package fouriernowruzmodel

import (
	"errors"
	activations "gitlab.com/john-byte/gopher-learning/activation-funcs"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"

	deltawcalcmod "gitlab.com/john-byte/gopher-learning/delta-weight-calc/fourier-nowruz-net-calc"
	mathutils "gitlab.com/john-byte/gopher-learning/utils/math"
	matmod "gonum.org/v1/gonum/mat"
)

type TStandardFourierNowruzLayer struct {
	dim                int
	sumMatx            *matmod.Dense
	outputs            *matmod.Dense
	outputsCalced      bool
	amplitudes         *matmod.Dense
	frequencies        *matmod.Dense
	ampsFreqsCorrected bool
	errors             *matmod.Dense
	errorsCalced       bool
	energyPotential    float64
	deltaWeightCalc    *deltawcalcmod.TFourierNowruzNetCalc
	activationFn       activations.TActivationFunc
}

func NewStandardFourierNowruzLayer(
	dim int,
	energyPotential float64,
	initAmplitudes *matmod.Dense,
) *TStandardFourierNowruzLayer {
	aR, aC := initAmplitudes.Dims()

	frequencies := matmod.NewDense(
		aR,
		aC,
		nil,
	)
	for r := 0; r < aR; r += 1 {
		for c := 0; c < aC; c += 1 {
			freq := energyPotential / initAmplitudes.At(r, c)
			frequencies.Set(r, c, freq)
		}
	}

	zerosSum := make([]float64, dim)
	for i := range zerosSum {
		zerosSum[i] = 0.0
	}

	zerosOuts := make([]float64, dim)
	for i := range zerosOuts {
		zerosOuts[i] = 0.0
	}

	zerosErrs := make([]float64, dim)
	for i := range zerosErrs {
		zerosErrs[i] = 0.0
	}

	return &TStandardFourierNowruzLayer{
		dim: dim,
		sumMatx: matmod.NewDense(
			dim,
			1,
			zerosSum,
		),
		outputs: matmod.NewDense(
			dim,
			1,
			zerosOuts,
		),
		outputsCalced:      false,
		amplitudes:         initAmplitudes,
		frequencies:        frequencies,
		ampsFreqsCorrected: false,
		errors: matmod.NewDense(
			dim,
			1,
			zerosErrs,
		),
		errorsCalced:    false,
		deltaWeightCalc: deltawcalcmod.New(aR, aC),
		activationFn:    activations.Atanp,
	}
}

func (self *TStandardFourierNowruzLayer) CalcActivation(inputs *matmod.Dense) *matmod.Dense {
	if self.outputsCalced {
		return self.outputs
	}

	inpDim, _ := inputs.Dims()
	for r := 0; r < self.dim; r += 1 {
		for c := 0; c < inpDim; c += 1 {
			val := self.amplitudes.At(r, c) * mathutils.Sinp(
				self.frequencies.At(r, c)*inputs.At(c, 0),
			)
			res := self.sumMatx.At(r, 0) + val
			self.sumMatx.Set(r, 0, res)
		}
	}

	self.outputs.Apply(
		self.activationFn,
		self.sumMatx,
	)

	self.outputsCalced = true
	return self.outputs
}

func (self *TStandardFourierNowruzLayer) CalcError(targets *matmod.Dense) *matmod.Dense {
	if self.errorsCalced {
		return self.errors
	}

	if !self.outputsCalced {
		errorsutils.PanicError(
			"StandardFourierNowruzLayer",
			"CalcError",
			errors.New("Outputs is not calculated"),
		)
		return nil
	}

	self.errors.Sub(
		targets,
		self.outputs,
	)
	self.errorsCalced = true
	return self.errors
}

func (self *TStandardFourierNowruzLayer) CalcErrorFromNextLayer(
	amplitudes *matmod.Dense,
	errs *matmod.Dense,
) *matmod.Dense {
	if self.errorsCalced {
		return self.errors
	}

	if !self.outputsCalced {
		errorsutils.PanicError(
			"StandardFourierNowruzLayer",
			"CalcErrorFromNextLayer",
			errors.New("Outputs is not calculated"),
		)
		return nil
	}

	self.errors.Product(
		matmod.DenseCopyOf(amplitudes).T(),
		errs,
	)
	self.errorsCalced = true
	return self.errors
}

func (self *TStandardFourierNowruzLayer) CorrectAmpsFreqs(
	learningRate float64,
	inputs *matmod.Dense,
) *matmod.Dense {
	if self.ampsFreqsCorrected {
		return self.amplitudes
	}

	if !self.outputsCalced {
		errorsutils.PanicError(
			"StandardFourierNowruzLayer",
			"CorrectAmpsFreqs",
			errors.New("Outputs is not calculated"),
		)
		return nil
	}

	if !self.errorsCalced {
		errorsutils.PanicError(
			"StandardFourierNowruzLayer",
			"CorrectAmpsFreqs",
			errors.New("Error is already calculated"),
		)
		return nil
	}

	deltaAmplitude := self.deltaWeightCalc.CalcDeltaAmplitude(
		self.errors,
		self.sumMatx,
		self.frequencies,
		inputs,
		learningRate,
	)

	self.amplitudes.Add(
		self.amplitudes,
		deltaAmplitude,
	)

	ampR, ampC := self.amplitudes.Dims()
	for r := 0; r < ampR; r += 1 {
		for c := 0; c < ampC; c += 1 {
			freq := self.energyPotential / self.amplitudes.At(r, c)
			self.frequencies.Set(r, c, freq)
		}
	}

	self.ampsFreqsCorrected = true
	return self.amplitudes
}

func (self *TStandardFourierNowruzLayer) Reset() {
	self.outputsCalced = false
	self.errorsCalced = false
	self.ampsFreqsCorrected = false
}

func (self *TStandardFourierNowruzLayer) GetAmplitudes() *matmod.Dense {
	return self.amplitudes
}

func (self *TStandardFourierNowruzLayer) GetFrequencies() *matmod.Dense {
	return self.frequencies
}

func (self *TStandardFourierNowruzLayer) GetErrors() *matmod.Dense {
	return self.errors
}

func (self *TStandardFourierNowruzLayer) GetDim() int {
	return self.dim
}

func (self *TStandardFourierNowruzLayer) GetEnergyPotential() float64 {
	return self.energyPotential
}
