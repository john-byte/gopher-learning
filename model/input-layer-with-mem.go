package model

import (
	"errors"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"

	matmod "gonum.org/v1/gonum/mat"
)

type TInputLayerWithMem struct {
	dim       int
	memDim    int
	memBlocks int
	memPtr    int
	outputs   *matmod.Dense
}

func NewInputLayerWithMem(dim int, memDim int, memBlocks int) *TInputLayerWithMem {
	zeros := make([]float64, dim+memDim*memBlocks)
	for i := range zeros {
		zeros[i] = 0.0
	}

	return &TInputLayerWithMem{
		dim:       dim,
		memDim:    memDim,
		memBlocks: memBlocks,
		memPtr:    -1,
		outputs: matmod.NewDense(
			dim+memDim*memBlocks,
			1,
			zeros,
		),
	}
}

func (self *TInputLayerWithMem) CalcActivation(inputs *matmod.Dense, mem *matmod.Dense) *matmod.Dense {
	inpDim, _ := inputs.Dims()
	for r := 0; r < inpDim; r += 1 {
		self.outputs.Set(r, 0, inputs.At(r, 0))
	}

	if self.memPtr >= 0 {
		for r := 0; r < self.memDim; r += 1 {
			memPtr := self.dim + self.memDim*self.memPtr + r
			self.outputs.Set(memPtr, 0, mem.At(r, 0))
		}
	}

	return self.outputs
}

func (self *TInputLayerWithMem) ExpandMem() {
	self.memPtr = (self.memPtr + 1) % self.memBlocks
}

func (self *TInputLayerWithMem) CalcError(targets *matmod.Dense) *matmod.Dense {
	errorsutils.PanicError(
		"InputLayerWithMem",
		"CalcError",
		errors.New("Error can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayerWithMem) CalcErrorFromNextLayer(
	weights *matmod.Dense,
	outputs *matmod.Dense,
) *matmod.Dense {
	errorsutils.PanicError(
		"InputLayerWithMem",
		"CalcErrorFromNextLayer",
		errors.New("Error can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayerWithMem) CorrectWeights(
	learningRate float64,
	prevOutputs *matmod.Dense,
) *matmod.Dense {
	errorsutils.PanicError(
		"InputLayerWithMem",
		"CorrectWeights",
		errors.New("Error can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayerWithMem) ResetInput() {
	for r := 0; r < self.dim; r += 1 {
		self.outputs.Set(r, 0, 0.0)
	}
}

func (self *TInputLayerWithMem) ResetFullMem() {
	for r := 0; r < self.memDim*self.memBlocks; r += 1 {
		self.outputs.Set(self.dim+r, 0, 0.0)
	}
	self.memPtr = -1
}

func (self *TInputLayerWithMem) ResetMemAtCurrBlock() {
	if self.memPtr < 0 {
		return
	}

	for r := 0; r < self.memDim; r += 1 {
		self.outputs.Set(self.dim+self.memDim*self.memPtr+r, 0, 0.0)
	}
}

func (self *TInputLayerWithMem) GetWeights() *matmod.Dense {
	errorsutils.PanicError(
		"InputLayerWithMem",
		"GetWeights",
		errors.New("Weights can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayerWithMem) GetErrors() *matmod.Dense {
	errorsutils.PanicError(
		"InputLayerWithMem",
		"GetErrors",
		errors.New("Errors can't be calculated for input layer"),
	)
	return nil
}

func (self *TInputLayerWithMem) GetDim() int {
	return self.dim
}

func (self *TInputLayerWithMem) GetMemDim() int {
	return self.memDim
}

func (self *TInputLayerWithMem) GetMemBlocks() int {
	return self.memBlocks
}
