package model

import (
	"errors"
	"fmt"
	errorsutils "gitlab.com/john-byte/gopher-learning/utils/errors"
	"os"
	"path"

	matmod "gonum.org/v1/gonum/mat"
)

type TModel struct {
	layers       []TLayer
	learningRate float64
	modelPath    string
}

func NewModel(
	layers []TLayer,
	learningRate float64,
	modelPath string,
) *TModel {
	if len(layers) < 2 {
		errorsutils.PanicError(
			"Model",
			"NewModel",
			errors.New("Model should have at least input and processing layers\n"),
		)
		return nil
	}

	return &TModel{
		layers:       layers,
		learningRate: learningRate,
		modelPath:    modelPath,
	}
}

func (self *TModel) Train(
	inputData []float64,
	targetData []float64,
) {
	out := self.layers[0].CalcActivation(
		matmod.NewDense(
			len(inputData),
			1,
			inputData,
		),
	)
	for i := 1; i < len(self.layers); i += 1 {
		out = self.layers[i].CalcActivation(out)
	}

	targets := matmod.NewDense(len(targetData), 1, targetData)
	layerErr := self.layers[len(self.layers)-1].CalcError(targets)
	for i := len(self.layers) - 2; i > 0; i -= 1 {
		layerErr = self.layers[i].CalcErrorFromNextLayer(
			self.layers[i+1].GetWeights(),
			layerErr,
		)
	}

	for i := len(self.layers) - 1; i > 0; i -= 1 {
		self.layers[i].CorrectWeights(
			self.learningRate,
			self.layers[i-1].CalcActivation(nil),
		)
	}

	for i := len(self.layers) - 1; i >= 0; i -= 1 {
		self.layers[i].Reset()
	}
}

func (self *TModel) Predict(inputData []float64) matmod.Matrix {
	out := self.layers[0].CalcActivation(
		matmod.NewDense(
			len(inputData),
			1,
			inputData,
		),
	)
	for i := 1; i < len(self.layers); i += 1 {
		out = self.layers[i].CalcActivation(out)
	}

	res := matmod.DenseCopyOf(out)

	for _, l := range self.layers {
		l.Reset()
	}

	return res
}

func (self *TModel) Save() error {
	for i, l := range self.layers {
		if i == 0 {
			continue
		}

		modelFileName := fmt.Sprintf(
			"layer-%v.dat",
			i+1,
		)
		modelPath := path.Join(
			self.modelPath,
			modelFileName,
		)
		file, err := os.Create(modelPath)
		defer file.Close()

		if err != nil {
			return errorsutils.LogError(
				"Model",
				"Save",
				err,
			)
		}
		l.GetWeights().MarshalBinaryTo(file)
	}

	return nil
}

func (self *TModel) Load() error {
	for i, l := range self.layers {
		if i == 0 {
			continue
		}

		modelFileName := fmt.Sprintf(
			"layer-%v.dat",
			i+1,
		)
		modelPath := path.Join(
			self.modelPath,
			modelFileName,
		)
		file, err := os.Open(modelPath)
		defer file.Close()

		if err != nil {
			return errorsutils.LogError(
				"Model",
				"Load",
				err,
			)
		}

		l.Reset()
		l.GetWeights().Reset()
		l.GetWeights().UnmarshalBinaryFrom(file)
	}

	return nil
}

func (self *TModel) GetLayers() []TLayer {
	return self.layers
}
