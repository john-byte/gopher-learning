package model

import (
	matmod "gonum.org/v1/gonum/mat"
)

type TLayer interface {
	CalcActivation(
		inputs *matmod.Dense,
	) *matmod.Dense
	CalcError(
		targets *matmod.Dense,
	) *matmod.Dense
	CalcErrorFromNextLayer(
		weights *matmod.Dense,
		errors *matmod.Dense,
	) *matmod.Dense
	CorrectWeights(
		learningRate float64,
		prevOutputs *matmod.Dense,
	) *matmod.Dense
	Reset()
	GetWeights() *matmod.Dense
	GetErrors() *matmod.Dense
	GetDim() int
}
